#$1 docker username
#$2 docker password
#$3 commit hash
#$4 container name
#$5 network
#$6 exposed port
#$7 internal port

echo "::::::::::::: DOCKER LOGIN :::::::::::::"
sudo docker login --username $1 --password $2

echo "::::::::::::: GET DOCKER IMAGE :::::::::::::"
sudo docker pull $1/$4:$3

echo "::::::::::::: LIST IMAGES :::::::::::::"
sudo docker images

echo "::::::::::::: RUN DOCKER CONTAINER :::::::::::::"
#https://stackoverflow.com/a/44364288
sudo docker ps -q --filter "name=$4" | grep -q . && docker stop $4 && docker rm $4
#-v ~/config:/app/config \
sudo docker run -d --restart always --name $4 \
--network $5 \
--publish $6:$7 \
$1/$4:$3;

echo "::::::::::::: DELETE ALL UNUSED IMAGES :::::::::::::"
sudo docker image prune -a -f

echo "::::::::::::: LIST CONTAINERS :::::::::::::"
sudo docker ps -a