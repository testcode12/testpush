FROM node:16

WORKDIR /app

COPY packag*.json ./

RUN npm i

COPY . .

EXPOSE 3001

CMD ["npm","run","dev"]