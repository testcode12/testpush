import {Router, Request, Response} from 'express';

const router = Router();

/**
   * @swagger
   * tags:
   *   - name: Login
   *     description: Login
   *   - name: Users
   *     description: User management and login
*/


/**
   * @swagger
   * /:
   *   get:
   *     description: Returns the homepage
   *     tags: [Login]
   *     responses:
   *       200:
   *         description: hello world
*/
router.get('/', (req:Request, res:Response) => {
    res.send('hellog word!')
})

/**
   * @swagger
   * /prueba:
   *   post:
   *     description: return request data
   *     tags: [Users, Login]
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: username
   *         description: User's username.
   *         in: formData
   *         required: false
   *         type: string
   *       - name: password
   *         description: User's password.
   *         in: formData
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: hello world
   *       400:
   *         description: Herror de algo
*/
router.post('/prueba', (req:Request, res:Response) => {
    if(!req.body.username){
        res.status(400).send("Falta el usuario")
    }else{
        res.send('hellog word!')
    }
})


export default router;
