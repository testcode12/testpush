import {openapiSpecification} from "./config/suagger";
import router from "./routes/muestra.routes";

const express = require('express');
const swaggerUi = require('swagger-ui-express');
const app = express();


app.use(express.json());
app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));
app.use('/api',router);

app.get('/doc-test', (req,res)=>{
    res.setHeader('Content-Type', 'application/json');
    res.send(openapiSpecification);
})

app.use(`/docs`, swaggerUi.serve, swaggerUi.setup(openapiSpecification));



app.listen(3001, () => {
    console.log('server iniciado')
})