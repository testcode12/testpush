const swaggerJsdoc = require('swagger-jsdoc');
import path from 'path';

const routesPath = path.resolve(__dirname,'../') + `/routes/*.routes.js`;

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Hello World',
            version: '1.0.0',
        },
        servers: [
            {
                "url":"http://localhost:{port}{basePath}",
                "description":"Local Test Server",
                "variables":{
                    "port":{
                    "enum":["3001"],
                        "default":"3001"
                    },
                    "basePath":{
                        "default":"/api"
                    }
                }
            }
        ]
    },
    apis: [routesPath],
};

export const openapiSpecification = swaggerJsdoc(options);